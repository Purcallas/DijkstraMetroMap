v 1.0
=====


Mars metro allows user to find the best path in a graph (Metro Map) using Dijkstra algorithm

Uses Dagger2 to improve the modularity
Uses ButterKnife to have more readable code by binding the android resources easily
Uses PhotoView to allow user to interact (by zoom and touch) the images.