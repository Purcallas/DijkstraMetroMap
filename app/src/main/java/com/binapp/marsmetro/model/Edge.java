package com.binapp.marsmetro.model;

/**
 * Edge
 *
 * Represents the edge between two stations
 *
 * Created by victor on 7/4/17.
 */

public class Edge  {
    private final Station source;       // Station (node) source
    private final Station destination;  // Station (node) destination
    private final Station direction;    // The last Station of the line
    private final int weight;           // The weight of the edge
    private final String line;          // The string metro line name


    public Edge(Station source, Station destination, Station direction, int weight, String line) {
        this.source = source;
        this.destination = destination;
        this.direction = direction;
        this.weight = weight;
        this.line = line;
    }

    public Station getDestination() {
        return destination;
    }
    public Station getSource() {
        return source;
    }
    public Station getDirection() { return direction; };
    public int getWeight() {
        return weight;
    }
    public String getLine() { return line; }

    @Override
    public String toString() {
        return source + " - " + destination;
    }


}
