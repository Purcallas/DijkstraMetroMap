package com.binapp.marsmetro.model;

/**
 * Metro Map
 *
 * Can be seen as a graph that contains the Station (nodes) and the Edges
 *
 * Created by victor on 7/4/17.
 */

import java.util.List;

public class MetroMap {
    private final List<Station> stations;       //Stations or nodes
    private final List<Edge> edges;             //Edges between stations

    public MetroMap(List<Station> stations, List<Edge> edges) {
        this.stations = stations;
        this.edges = edges;
    }

    public List<Station> getStations() {
        return stations;
    }

    public List<Edge> getEdges() {
        return edges;
    }



}
