package com.binapp.marsmetro.model;

import java.util.List;


/**
 * Station
 *
 * Represents a metro station. But can be seen as a node in a graph (the metro map is a graph)
 * Created by victor on 7/4/17.
 */

public class Station {
    final private String name;              // Station name
    final private List<String> lines;       // List of String metro line names


    public Station(String name, List<String> lines) {
        this.name = name;
        this.lines = lines;
    }
    public String getName() {
        return name;
    }
    public List getId() {
        return lines;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Station other = (Station) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
