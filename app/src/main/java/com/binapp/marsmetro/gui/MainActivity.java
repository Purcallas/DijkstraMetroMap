package com.binapp.marsmetro.gui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.binapp.marsmetro.R;
import com.binapp.marsmetro.controller.PathController;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Main Activity class
 *
 * Is the Main interface following the MVC pattern.
 * Uses the controller PathController execute the logic.
 *
 * Only send the Station names to the PathController and receive a message to be showed
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    @BindView(R.id.source_spinner)
    Spinner sourceSpinner;
    @BindView(R.id.destination_spinner)
    Spinner destinationSpinner;
    @BindView(R.id.find_path_button)
    FloatingActionButton findPathButton;

    private PathController pathController;           // The controller who manages the best path finder

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Add butterKnife to bind the resources easily
        ButterKnife.bind(this);

        //Action Bar Style
        if(getSupportActionBar() != null){
            //getSupportActionBar().setDisplayUseLogoEnabled(true);
            //getSupportActionBar().setDisplayShowHomeEnabled(true);
            //getSupportActionBar().setIcon(R.drawable.ic_launcher);
            getSupportActionBar().setTitle("");
        }


        //Initialize the pathController
        pathController = new PathController();

        //Give content to the spinners with the name of the stations
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, pathController.getStationsNamesList()); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sourceSpinner.setAdapter(spinnerArrayAdapter);
        destinationSpinner.setAdapter(spinnerArrayAdapter);

        //Set listeners
        findPathButton.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        pathController.setFirstStationName(sourceSpinner.getSelectedItem().toString());
        pathController.setSecondStationName(destinationSpinner.getSelectedItem().toString());

        new AlertDialog.Builder(this)
                .setTitle("The Way")
                .setMessage(pathController.getMessage())
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }
}
