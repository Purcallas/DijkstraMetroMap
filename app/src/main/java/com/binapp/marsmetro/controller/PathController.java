package com.binapp.marsmetro.controller;


import android.util.Pair;

import com.binapp.marsmetro.controller.algorithms.DijkstraAlgorithm;
import com.binapp.marsmetro.controller.algorithms.PathAlgorithm;
import com.binapp.marsmetro.data.DaggerMarsMetroDataComponent;
import com.binapp.marsmetro.data.MarsMetroDataComponent;
import com.binapp.marsmetro.model.Edge;
import com.binapp.marsmetro.model.Station;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;



/**
 * PathController.java
 *
 * The Path Controller class are in the middle between model and view to provide the view all
 * the necessary data.
 *
 * This controller uses Dagger2 to get the Mars Metro Map and uses the PathAlgoritm interface to
 * calculate the closest way between to stations to retrieve this info to the user
 *
 * Created by victor on 8/4/17.
 */

public class PathController {

    //STATIC VARIABLES
    private static final int TIME_BETWEEN_STATIONS = 5;
    private static final int STATION_COST = 1;
    private static final int STATION_CHANGE_COST = 1;


    private MarsMetroDataComponent marsMetroDataComponent;
    private PathAlgorithm pathAlgorithm;
    private LinkedList<Station> stationsPath;
    public LinkedList<Edge> edgePath;       //public for testing



    /*****************
     * CONSTRUCTOR
     *****************/
    public PathController() {
        //Using Dagger 2 to get the Data component
        this.marsMetroDataComponent = DaggerMarsMetroDataComponent.builder().build();
        //Using the Dijkstra algorithm as actual path algorithm
        this.pathAlgorithm = new DijkstraAlgorithm(marsMetroDataComponent.getMetroMap());
    }

    /*****************
     * PUBLIC METHODS
     *****************/

    /**
     * Get all the stations names from the metro map sorted by name
     * @return List<String> the sorted station names
     */
    public List<String> getStationsNamesList(){
        List<String> stationNames = new ArrayList<>();
        for (Station station : this.marsMetroDataComponent.getMetroMap().getStations()){
            stationNames.add(station.getName());
        }
        Collections.sort(stationNames);
        return stationNames;
    }


    /**
     * Set the first station
     * It must be called before setSecondStationName
     *
     * Starts the execution of the algorithm
     *
     * @param sourceStationName Station, the source station
     */
    public void setFirstStationName(String sourceStationName){
        //Get the station from the name
        Station sourceStation = getStationFromName(sourceStationName);
        //Execute the algorithm
        //Dijkstra finds all the shortest routes to graph stations from the source station
        if (sourceStation != null) this.pathAlgorithm.execute(sourceStation);
    }

    /**
     * Set the second station.
     * Get the fastest paths (station and edge) from the algorithm
     * Fill the stations path, edge path, pathTime and pathCost
     *
     * To work properly it must be called after setFirstStationName
     *
     * @param destinationStationName the destination Station name
     */
    public void setSecondStationName(String destinationStationName){
        Station destinationStation = getStationFromName(destinationStationName);
        if (destinationStation != null) {
            this.stationsPath = this.pathAlgorithm.getStationsPath(destinationStation);
            this.edgePath = this.pathAlgorithm.getEdgesPath(this.stationsPath);
        }
    }

    /**
     * Builds a message about the Stations path to be displayed in the User Interface
     *
     *
     * @return a String with the message well formed
     */
    public String getMessage(){

        String message = "Congratulations, you are actually in your destination.\n If you are looking to travel to a parallel universe please buy the premium version.";
        if (edgePath != null && edgePath.size() > 0){
            message = "If you want to travel from " + stationsPath.getFirst().getName() + " to " + stationsPath.getLast().getName() + " then:\n\n";

            String pathMessage = "Take the " + this.edgePath.getFirst().getLine() + " at " + this.stationsPath.getFirst().getName()
                    + " moving thowards " + this.edgePath.getFirst().getDirection().getName() +". ";
            for (Pair<Station,Edge> stationChange : getStationChanges(this.edgePath, this.stationsPath)){
                Station station = stationChange.first;
                Edge edge = stationChange.second;
                pathMessage += "Get down at " + station.getName() + " and take the " + edge.getLine() + " moving thowards " + edge.getDirection().getName() +". ";
            }
            message+= pathMessage + "\n\n";

            message += "Time : It would take " + getTime(this.edgePath) + " minutes. \n";
            message += "Cost : " + getCost(this.edgePath) + " $\n";
            message += "Stops: " + this.edgePath.size()+ "\n";
            message += "Line changes : " + getLineChanges(this.edgePath)+"\n\n";

        }
        return message;
    }

    /*****************
     * PRIVATE METHODS
     *****************/

    /**
     * Get the station with the same name of the entry param
     * @param name String station name
     * @return Station with the same name
     */
    private Station getStationFromName(String name){
        for(Station station : marsMetroDataComponent.getMetroMap().getStations()){
            if (station.getName().equals(name)) return station;
        }
        return null;
    }

    /**
     * Simply get cost using from an edge Path
     * @param edgePath the path of Edge/s
     * @return int cost
     */
    private int getCost(LinkedList<Edge> edgePath) {
        if (edgePath == null) return 0;
        int cost = edgePath.size() * STATION_COST;
        String line;
        if (edgePath.size()>0){
            //Set inital line
            line = edgePath.get(0).getLine();
            for (int i = 1; i < edgePath.size();i++) {
                Edge edge = edgePath.get(i);
                // If line changes add aditional cost
                if (!line.equals(edge.getLine())) {
                    line = edge.getLine();
                    cost += STATION_CHANGE_COST;
                }
            }
        }
        return cost;
    }

    /**
     * Get the time using the edge path.
     * @param edgePath the path of Edge/s
     * @return int time
     */
    private int getTime(LinkedList<Edge> edgePath){
        if (edgePath == null) return 0;
        return this.edgePath.size() * TIME_BETWEEN_STATIONS;
    }



    /**
     * Get the number of line changes from a path of edges
     * @return int number of line changes
     */
    //Its public for testing
    public int getLineChanges(LinkedList<Edge> edgePath){
        int lineChanges = 0;
        String actualLine = edgePath.getFirst().getLine();
        for (Edge edge : edgePath){
            if (!edge.getLine().equals(actualLine)){
                lineChanges++;
                actualLine = edge.getLine();
            }
        }
        return lineChanges;
    }


    /**
     * Get the Array of Pairs (Station, Edge) where in the path the line are changing
     * @param edgePath  the paths of Edge in a Linked List
     * @param stationsPath the paths of Station in a Linked List
     * @return ArrayList(Pair of Station,Edges) an arrayList with a pair (station,edge) with all the line changes
     */
    private ArrayList<Pair<Station,Edge>> getStationChanges(LinkedList<Edge> edgePath,LinkedList<Station> stationsPath){
        ArrayList<Pair<Station,Edge>> stationChanges = new ArrayList<>();
        String actualLine = edgePath.getFirst().getLine();
        for (int i = 0 ; i < edgePath.size() ; i++){
            Edge edge = edgePath.get(i);
            if (!edge.getLine().equals(actualLine)){
                actualLine = edge.getLine();
                Pair<Station,Edge> stationChange = new Pair<>(stationsPath.get(i), edge);
                stationChanges.add(stationChange);
            }
        }
        return stationChanges;
    }

}
