package com.binapp.marsmetro.controller.algorithms;

import com.binapp.marsmetro.model.Edge;
import com.binapp.marsmetro.model.MetroMap;
import com.binapp.marsmetro.model.Station;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Dijkstra Algorithm class
 *
 * This is a common Dijkstra algorithm using Stations as nodes, and all the edges that conforms the
 * graph or metro map in this case
 *
 * It uses the PathAlgorithm interface to be modular and replaceable without affecting the code
 *
 * Created by victor on 7/4/17.
 */

public class DijkstraAlgorithm implements PathAlgorithm {

    //Private variables
    private final List<Edge> edges;             //All the edges from the MetroMap
    private Set<Station> settledStations;       //A list with the analyzed by algorithm Stations
    private Set<Station> unSettledStations;     //A list with the non analyzed by algorithm Stations
    private Map<Station, Station> predecessors; //The previous Stations connected forming a path
    private Map<Station, Integer> distance;     //The distance between Stations and the source station

    /*****************
     * CONSTRUCTOR
     *****************/

    /**
     * Constructor
     * @param metroMap as Graph of edges
     */
    public DijkstraAlgorithm(MetroMap metroMap) {
        // create a copy of the array so that we can operate on this array
        this.edges = new ArrayList<>(metroMap.getEdges());
    }

    /*****************
     * PUBLIC METHODS
     *****************/

    /**
     * It will analyze and store all the distances between the param source station and all the rest of Stations
     * if they are connected by edges (directly or not)
     * @param sourceStation the source station
     */
    public void execute(Station sourceStation) {
        //init the variables
        settledStations = new HashSet<>();
        unSettledStations = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        //add first station
        distance.put(sourceStation, 0);
        unSettledStations.add(sourceStation);
        while (unSettledStations.size() > 0) {
            Station minimumDistantNeighbourStation = getMinimum(unSettledStations);
            settledStations.add(minimumDistantNeighbourStation);
            unSettledStations.remove(minimumDistantNeighbourStation);
            findMinimalDistances(minimumDistantNeighbourStation);
        }
    }

    /**
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     * @param target Station target
     * @return the List of stations between the source Station to this target
     */
    public LinkedList<Station> getStationsPath(Station target) {
        LinkedList<Station> path = new LinkedList<>();
        Station step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

    /**
     * This method returns the path of edges using a path of stations (LinkedList<Stations>)
     * @param stations Station target
     * @return the List of edges that links a path of stations
     */
    public LinkedList<Edge> getEdgesPath(LinkedList<Station> stations){
        LinkedList<Edge> edgePath = new LinkedList<>();

        if (stations == null) return edgePath;

        for (int i = 0 ; i < stations.size() - 1 ; i++ ){
            Station stationOne = stations.get(i);
            Station stationTwo = stations.get(i+1);
            Edge edge = getEdge(stationOne,stationTwo);
            edgePath.add(edge);
        }
        return edgePath;
    }

    /*****************
     * PRIVATE METHODS
     *****************/

    /**
     * Fin the minimal distances from one station to his neighbors
     * Fill the next variables
     * distance, predecessors, unSettled distances
     * @param station is the station to analyze
     */
    private void findMinimalDistances(Station station) {
        List<Station> adjacentStations = getNeighbors(station);
        for (Station adjacentTargetStation : adjacentStations) {
            if (getShortestDistance(adjacentTargetStation) > getShortestDistance(station)
                    + getDistance(station, adjacentTargetStation)) {
                distance.put(adjacentTargetStation, getShortestDistance(station)
                        + getDistance(station, adjacentTargetStation));
                predecessors.put(adjacentTargetStation, station);
                unSettledStations.add(adjacentTargetStation);
            }
        }

    }

    /**
     * Get the distance between two stations if they has a linking edge
     * @param initial station
     * @param target station
     * @return the edge weight between two stations
     */
    private int getDistance(Station initial, Station target) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(initial)
                    && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    /**
     * Get the Edge between two stations
     * @param initial station
     * @param target station
     * @return Edge between stations
     */
    private Edge getEdge(Station initial, Station target) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(initial)
                    && edge.getDestination().equals(target)) {
                return edge;
            }
        }
        throw new RuntimeException("Should not happen");
    }

    /**
     * Get the List of Stations that are Station parameter neighbors
     * @param station Station to be analyzed
     * @return List<Station> stations that are neighbors
     */
    private List<Station> getNeighbors(Station station) {
        List<Station> neighbors = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(station)
                    && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    /**
     * Get the station with the minimum distance from the source Station
     * @param stations Set of stations
     * @return the station with the minimum distance
     */
    private Station getMinimum(Set<Station> stations) {
        Station minimum = null;
        for (Station station : stations) {
            if (minimum == null) {
                minimum = station;
            } else {
                if (getShortestDistance(station) < getShortestDistance(minimum)) {
                    minimum = station;
                }
            }
        }
        return minimum;
    }

    /**
     * Simply return true if the station are already analyzed by the Dijkstra algorithm
     * @param station Station to be analyzed
     * @return true if station are already analyzed
     */
    private boolean isSettled(Station station) {
        return settledStations.contains(station);
    }

    /**
     * Get de distance from this distances
     * (this.distance are filled from the Source Station in execute method)
     * @param destination Station destination station
     * @return int distance between the source station and the param destination Station
     */
    private int getShortestDistance(Station destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE; //If there are not distance, according Dijsktra the distance is infinite
        } else {
            return d;
        }
    }
}
