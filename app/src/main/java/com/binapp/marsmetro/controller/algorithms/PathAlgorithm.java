package com.binapp.marsmetro.controller.algorithms;


import com.binapp.marsmetro.model.Edge;
import com.binapp.marsmetro.model.Station;

import java.util.LinkedList;

/**
 * Path Algorithm interface must be added in your Path Algorithms to ensure that they fit
 * in the rest of the code
 *
 * Created by victor on 9/4/17.
 */

public interface PathAlgorithm {

    /**
     * It will stars the algorithm calculation, about the best ways to arrive the rest
     * of Stations using the source station
     * @param sourceStation the source station
     */
    void execute(Station sourceStation);

    /**
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     * @param target Station target
     * @return the List of stations between the source Station to this target
     */
    LinkedList<Station> getStationsPath(Station target);

    /**
     * This method returns the path of edges using a path of stations (LinkedList<Stations>)
     * @param stations Station target
     * @return the List of edges that links a path of stations
     */
    LinkedList<Edge> getEdgesPath(LinkedList<Station> stations);
}
