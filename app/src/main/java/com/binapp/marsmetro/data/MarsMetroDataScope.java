package com.binapp.marsmetro.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * All the classes that uses this scope will be created once, as Singleton.
 * Created by victor on 8/4/17.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
@interface MarsMetroDataScope {
}
