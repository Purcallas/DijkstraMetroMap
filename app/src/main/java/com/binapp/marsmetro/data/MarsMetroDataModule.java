package com.binapp.marsmetro.data;

import com.binapp.marsmetro.model.Edge;
import com.binapp.marsmetro.model.MetroMap;
import com.binapp.marsmetro.model.Station;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dagger.Module;
import dagger.Provides;

/**
 * Mars Metro Data Module
 *
 * Its a Dagger2 Module that fills all the data wich is necessary for the application
 *
 * Created by victor on 7/4/17.
 */

@Module
class MarsMetroDataModule {

    @Provides
    @MarsMetroDataScope
    MetroMap metroMap(){
        final int STATION_COMMON_DISTANCE = 1;
        final String LINE_RED = "Red Line";
        final String LINE_GREEN = "Green Line";
        final String LINE_BLUE = "Blue Line";
        final String LINE_YELLOW = "Yellow Line";
        final String LINE_BLACK = "Black Line";

        //Build Stations
        Station matrixStand = new Station("Matrix Stand", Collections.singletonList(LINE_RED));
        Station keymakersLane = new Station("Keymakers Lane", Collections.singletonList(LINE_RED));
        Station oracleLane = new Station("Oracle Lane", Collections.singletonList(LINE_RED));
        Station cypheLane = new Station("Cypher Lane", Collections.singletonList(LINE_RED));
        Station smithLane = new Station("Smith Lane", Collections.singletonList(LINE_RED));
        Station trinityLane = new Station("Trinity Lane", Collections.singletonList(LINE_RED));
        Station northPark = new Station("North Park",Collections.singletonList(LINE_GREEN));
        Station sheldonStreet = new Station("Sheldon Street",Collections.singletonList(LINE_GREEN));
        Station greenland = new Station("Greenland",Collections.singletonList(LINE_GREEN));
        Station stadiumHouse = new Station("Stadium House",Collections.singletonList(LINE_GREEN));
        Station greenHouse = new Station("Green House",Collections.singletonList(LINE_GREEN));
        Station southPole =  new Station("South Pole",Collections.singletonList(LINE_GREEN));
        Station footStand = new Station("Foot Stand",Collections.singletonList(LINE_BLUE));
        Station footballStadium = new Station("Football Stadium",Collections.singletonList(LINE_BLUE));
        Station peterPark = new Station("Peter Park",Collections.singletonList(LINE_BLUE));
        Station maximus = new Station("Maximus",Collections.singletonList(LINE_BLUE));
        Station rockyStreet = new Station("Rocky Street",Collections.singletonList(LINE_BLUE));
        Station boxersStreet = new Station("Boxers Street",Collections.singletonList(LINE_BLUE));
        Station westEnd = new Station("West End",Collections.singletonList(LINE_BLUE));
        Station orangeStreet = new Station("Orange Street",Collections.singletonList(LINE_YELLOW));
        Station silkBoard = new Station("Silk Board",Collections.singletonList(LINE_YELLOW));
        Station snakePark = new Station("Snake Park",Collections.singletonList(LINE_YELLOW));
        Station littleStreet = new Station("Little Street",Collections.singletonList(LINE_YELLOW));
        Station cricketGrounds = new Station("Cricket Grounds",Collections.singletonList(LINE_YELLOW));
        Station gothamStreet = new Station("Gotham Street",Collections.singletonList(LINE_BLACK));
        Station batmanStreet = new Station("Batman Street",Collections.singletonList(LINE_BLACK));
        Station jokersStreet = new Station("Jokers Street",Collections.singletonList(LINE_BLACK));
        Station hawkinsStreet = new Station("Hawkins Street",Collections.singletonList(LINE_BLACK));
        Station daVinciLane = new Station("Da Vinci Lane",Collections.singletonList(LINE_BLACK));
        Station newtonBathTub = new Station("Newton Bath Tub",Collections.singletonList(LINE_BLACK));
        Station einsteinLane = new Station("Einstein Lane",Collections.singletonList(LINE_BLACK));
        Station boxingAvenue = new Station("Boxing Avenue", Arrays.asList(LINE_RED,LINE_BLUE));
        Station morpheusLane = new Station("Morpheus Lane", Arrays.asList(LINE_RED,LINE_YELLOW));
        Station neoLane = new Station("Neo Lane", Arrays.asList(LINE_RED,LINE_BLACK));
        Station cityCentre = new Station("City Centre",Arrays.asList(LINE_GREEN,LINE_BLUE));
        Station greenCross = new Station("Green Cross",Arrays.asList(LINE_GREEN,LINE_YELLOW));
        Station southPark = new Station("South Park",Arrays.asList(LINE_GREEN,LINE_BLACK));
        Station eastEnd = new Station("East End",Arrays.asList(LINE_BLUE,LINE_BLACK));

        //Lines with direction
        Map<String,Station[]> lines = new HashMap<>();
        lines.put(LINE_RED,new Station[]{neoLane,matrixStand});
        lines.put(LINE_GREEN,new Station[]{southPark,northPark});
        lines.put(LINE_BLUE,new Station[]{westEnd,eastEnd});
        lines.put(LINE_YELLOW,new Station[]{cricketGrounds,greenCross});
        lines.put(LINE_BLACK,new Station[]{neoLane,eastEnd});

        //Fill Stations
        List<Station> stations = new ArrayList<>();
        stations.add(matrixStand);
        stations.add(keymakersLane);
        stations.add(oracleLane);
        stations.add(cypheLane);
        stations.add(smithLane);
        stations.add(trinityLane);
        stations.add(northPark);
        stations.add(sheldonStreet);
        stations.add(greenland);
        stations.add(stadiumHouse);
        stations.add(greenHouse);
        stations.add(southPole);
        stations.add(footStand);
        stations.add(footballStadium);
        stations.add(peterPark);
        stations.add(maximus);
        stations.add(rockyStreet);
        stations.add(boxersStreet);
        stations.add(westEnd);
        stations.add(orangeStreet);
        stations.add(silkBoard);
        stations.add(snakePark);
        stations.add(littleStreet);
        stations.add(cricketGrounds);
        stations.add(gothamStreet);
        stations.add(batmanStreet);
        stations.add(jokersStreet);
        stations.add(hawkinsStreet);
        stations.add(daVinciLane);
        stations.add(newtonBathTub);
        stations.add(einsteinLane);
        stations.add(boxingAvenue);
        stations.add(morpheusLane);
        stations.add(neoLane);
        stations.add(cityCentre);
        stations.add(greenCross);
        stations.add(southPark);
        stations.add(eastEnd);


        //Build and fill edges
        List<Edge> edges = new ArrayList<>();
        addBothSensesEdge(edges, lines, matrixStand,keymakersLane, STATION_COMMON_DISTANCE, LINE_RED);
        addBothSensesEdge(edges, lines, keymakersLane,oracleLane, STATION_COMMON_DISTANCE, LINE_RED);
        addBothSensesEdge(edges, lines, oracleLane,boxingAvenue, STATION_COMMON_DISTANCE, LINE_RED);
        addBothSensesEdge(edges, lines, boxingAvenue,cypheLane, STATION_COMMON_DISTANCE, LINE_RED);
        addBothSensesEdge(edges, lines, cypheLane,smithLane, STATION_COMMON_DISTANCE, LINE_RED);
        addBothSensesEdge(edges, lines, smithLane,morpheusLane, STATION_COMMON_DISTANCE, LINE_RED);
        addBothSensesEdge(edges, lines, morpheusLane,trinityLane, STATION_COMMON_DISTANCE, LINE_RED);
        addBothSensesEdge(edges, lines, trinityLane,neoLane, STATION_COMMON_DISTANCE, LINE_RED);

        addBothSensesEdge(edges, lines, northPark,sheldonStreet, STATION_COMMON_DISTANCE, LINE_GREEN);
        addBothSensesEdge(edges, lines, sheldonStreet,greenland, STATION_COMMON_DISTANCE, LINE_GREEN);
        addBothSensesEdge(edges, lines, greenland,cityCentre, STATION_COMMON_DISTANCE, LINE_GREEN);
        addBothSensesEdge(edges, lines, cityCentre,stadiumHouse, STATION_COMMON_DISTANCE, LINE_GREEN);
        addBothSensesEdge(edges, lines, stadiumHouse,greenHouse, STATION_COMMON_DISTANCE, LINE_GREEN);
        addBothSensesEdge(edges, lines, greenHouse,greenCross, STATION_COMMON_DISTANCE, LINE_GREEN);
        addBothSensesEdge(edges, lines, greenCross,southPole, STATION_COMMON_DISTANCE, LINE_GREEN);
        addBothSensesEdge(edges, lines, southPole,southPark, STATION_COMMON_DISTANCE, LINE_GREEN);

        addBothSensesEdge(edges, lines, eastEnd,footStand, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, footStand,footballStadium, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, footballStadium,cityCentre, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, cityCentre,peterPark, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, peterPark,maximus, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, maximus,rockyStreet, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, rockyStreet,boxersStreet, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, boxersStreet,boxingAvenue, STATION_COMMON_DISTANCE, LINE_BLUE);
        addBothSensesEdge(edges, lines, boxingAvenue,westEnd, STATION_COMMON_DISTANCE, LINE_BLUE);

        addBothSensesEdge(edges, lines, greenCross,orangeStreet, STATION_COMMON_DISTANCE, LINE_YELLOW);
        addBothSensesEdge(edges, lines, orangeStreet,silkBoard, STATION_COMMON_DISTANCE, LINE_YELLOW);
        addBothSensesEdge(edges, lines, silkBoard,snakePark, STATION_COMMON_DISTANCE, LINE_YELLOW);
        addBothSensesEdge(edges, lines, snakePark,morpheusLane, STATION_COMMON_DISTANCE, LINE_YELLOW);
        addBothSensesEdge(edges, lines, morpheusLane,littleStreet, STATION_COMMON_DISTANCE, LINE_YELLOW);
        addBothSensesEdge(edges, lines, littleStreet,cricketGrounds, STATION_COMMON_DISTANCE, LINE_YELLOW);

        addBothSensesEdge(edges, lines, eastEnd,gothamStreet, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, gothamStreet,batmanStreet, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, batmanStreet,jokersStreet, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, jokersStreet,hawkinsStreet, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, hawkinsStreet,daVinciLane, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, daVinciLane,southPark, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, southPark,newtonBathTub, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, newtonBathTub,einsteinLane, STATION_COMMON_DISTANCE, LINE_BLACK);
        addBothSensesEdge(edges, lines, einsteinLane,neoLane, STATION_COMMON_DISTANCE, LINE_BLACK);

    //Return the MetroMap
    return new MetroMap(stations,edges);
}

    private void addBothSensesEdge(List<Edge> edges, Map<String,Station[]> lines, Station stationOne, Station stationTwo, int distance, String line) {
        edges.add(new Edge(stationOne,stationTwo, lines.get(line)[0], distance , line));
        edges.add(new Edge(stationTwo,stationOne, lines.get(line)[1], distance , line));
    }


}
