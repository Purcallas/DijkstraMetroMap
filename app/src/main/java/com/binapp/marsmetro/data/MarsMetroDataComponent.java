package com.binapp.marsmetro.data;

import com.binapp.marsmetro.model.MetroMap;

import dagger.Component;

/**
 * Mars Metro Data Component
 *
 * Is an Dagger to improve the modularity to get the data of the proyect from different sources
 * Its linked with the Mars Metro Data Module
 *
 * Created by victor on 7/4/17.
 */

@MarsMetroDataScope
@Component (modules = MarsMetroDataModule.class)
public interface MarsMetroDataComponent {

    MetroMap getMetroMap();

}
