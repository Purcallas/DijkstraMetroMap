package com.binapp.marsmetro;

import android.util.Log;

import com.binapp.marsmetro.controller.PathController;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Check the integrity of the Hardcoded Data
 *
 * Created by victor on 10/4/17.
 */

public class TestDataIntegrity {

    @Test
    public void checkHardCodedDataLineRedIntegrity(){
        PathController pathController = new PathController();

        //TEST LINE RED
        pathController.setFirstStationName("Matrix Stand");
        pathController.setSecondStationName("Neo Lane");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        pathController.setFirstStationName("Neo Lane");
        pathController.setSecondStationName("Matrix Stand");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 8);

    }

    @Test
    public void checkHardCodedDataLineBlueIntegrity(){
        PathController pathController = new PathController();

        //TEST LINE BLUE
        pathController.setFirstStationName("East End");
        pathController.setSecondStationName("West End");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        pathController.setFirstStationName("West End");
        pathController.setSecondStationName("East End");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 9);

    }

    @Test
    public void checkHardCodedDataLineGreenIntegrity(){
        PathController pathController = new PathController();

        //TEST LINE GREEN
        pathController.setFirstStationName("North Park");
        pathController.setSecondStationName("South Park");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        pathController.setFirstStationName("South Park");
        pathController.setSecondStationName("North Park");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 8);

    }

    @Test
    public void checkHardCodedDataLineYellowIntegrity(){
        PathController pathController = new PathController();

        //TEST LINE YELLOW
        pathController.setFirstStationName("Green Cross");
        pathController.setSecondStationName("Cricket Grounds");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        pathController.setFirstStationName("Cricket Grounds");
        pathController.setSecondStationName("Green Cross");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 6);

    }

    @Test
    public void checkHardCodedDataLineBlackIntegrity(){
        PathController pathController = new PathController();

        //TEST LINE BLACK
        pathController.setFirstStationName("East End");
        pathController.setSecondStationName("Neo Lane");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        pathController.setFirstStationName("Neo Lane");
        pathController.setSecondStationName("East End");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 0);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 9);

    }

    @Test
    public void checkHardCodedDataRouteIntegrity(){
        PathController pathController = new PathController();

        //TEST ROUTE 1
        pathController.setFirstStationName("Stadium House");
        pathController.setSecondStationName("East End");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 1);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 4);

        //TEST ROUTE 1 REVERSE

        pathController.setFirstStationName("East End");
        pathController.setSecondStationName("Stadium House");
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 1);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 4);


        //TEST ROUTE 2


        pathController.setFirstStationName("Hawkins Street");
        pathController.setSecondStationName("Little Street");
        Log.e("LOG","" + pathController.getLineChanges(pathController.edgePath));
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 2);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 8);

        pathController.setFirstStationName("Hawkins Street");
        pathController.setSecondStationName("Little Street");
        Log.e("LOG","" + pathController.getLineChanges(pathController.edgePath));
        assertTrue(pathController.getLineChanges(pathController.edgePath) == 2);

        //Number of edges
        assertTrue(pathController.edgePath.size() == 8);

    }
}
