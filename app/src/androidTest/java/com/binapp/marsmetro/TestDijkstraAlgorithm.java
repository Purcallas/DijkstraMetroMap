package com.binapp.marsmetro;

import com.binapp.marsmetro.controller.PathController;
import com.binapp.marsmetro.controller.algorithms.DijkstraAlgorithm;
import com.binapp.marsmetro.model.Edge;
import com.binapp.marsmetro.model.MetroMap;
import com.binapp.marsmetro.model.Station;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Test the Dijkstra Algorithm
 * Created by victor on 7/4/17.
 */

public class TestDijkstraAlgorithm {

    private List<Station> stations;
    private List<Edge> edges;


    @Test
    public void testExcute() {
        stations = new ArrayList<>();
        edges = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            Station location = new Station("Station_" + i, Collections.singletonList(""));
            stations.add(location);
        }

        addLane(0, 1, 85);
        addLane(0, 2, 217);
        addLane(0, 4, 173);
        addLane(2, 6, 186);
        addLane(2, 7, 103);
        addLane(3, 7, 183);
        addLane(5, 8, 250);
        addLane(8, 9, 84);
        addLane(7, 9, 167);
        addLane(4, 9, 502);
        addLane(9, 10, 40);
        addLane(1, 10, 600);

        // Lets check from location Loc_1 to Loc_10
        MetroMap metroMap = new MetroMap(stations, edges);
        DijkstraAlgorithm dijkstraAlgorithm = new DijkstraAlgorithm(metroMap);
        dijkstraAlgorithm.execute(stations.get(0));
        LinkedList<Station> path = dijkstraAlgorithm.getStationsPath(stations.get(10));

        assertNotNull(path);
        assertTrue(path.size() > 0);

        for (Station station : path) {
            System.out.println(station);
        }

    }


    private void addLane(int sourceLocStation, int destLocStation,
                         int duration) {
        Edge lane = new Edge(stations.get(sourceLocStation), stations.get(destLocStation), null, duration , null);
        edges.add(lane);
    }


}
